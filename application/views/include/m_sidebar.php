<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo base_url() ?>" class="brand-link">
        <img src="<?php echo base_url() ?>assets/dist/img/Bar.png" class="brand-image img-thumbnail"
             size="1.5">
        <span class="brand-text font-weight-light">Koombiyo_Shop</span>
    </a>

    <!-- Sidebar Menu -->
    <div>
        <nav class="nav-active">


            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->

                <li class="nav-item">
                    <a href="<?php echo base_url() ?>Dashboard" class="nav-link
                    <?php
                    if (isset($navi_active) && $navi_active == 'dashboard') {
                        echo 'active';
                    }
                    ?>
                       ">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>

                <li class="nav-item has-treeview <?php
                if (isset($navi_active) && $navi_active == 'product') {
                    echo 'menu-open';
                }
                ?>">
                    <a href="<?php echo base_url() ?>product" class="nav-link <?php
                    if (isset($navi_active) && $navi_active == 'product') {
                        echo 'active';
                    }
                    ?>">
                        <i class="nav-icon fab fa-product-hunt"></i>
                        <p>
                            Products
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview ">
                        <li class="nav-item">
                            <a href="<?php echo base_url() ?>products" class="nav-link
                            <?php
                            if (isset($navi_active) && $navi_active == 'products') {
                                echo 'active';
                            }
                            ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Products</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo base_url() ?>categories"class="nav-link  
                            <?php
                            if (isset($sub_navi_active) && $sub_navi_active == 'categories') {
                                echo 'active';
                            }
                            ?>
                               ">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Categories</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo base_url() ?>units" class="nav-link
                            <?php
                            if (isset($sub_navi_active) && $sub_navi_active == 'units') {
                                echo 'active';
                            }
                            ?>
                               ">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Units</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                <li class="nav-item">
                    <a href="<?php echo base_url() ?>customers" class="nav-link
                    <?php
                    if (isset($navi_active) && $navi_active == 'customers')
                        echo 'active';
                    ?>
                       ">
                        <i class="nav-icon fas fa-house-user"></i>
                        <p>
                            Customers
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?php echo base_url() ?>orders" class="nav-link
                    <?php
                    if (isset($navi_active) && $navi_active == 'orders')
                        echo 'active';
                    ?>
                       ">
                        <i class="nav-icon fab fa-first-order-alt"></i>
                        <p>
                            Orders
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="<?php echo base_url() ?>reports" class="nav-link
                    <?php
                    if (isset($navi_active) && $navi_active == 'reports')
                        echo 'active';
                    ?>
                       ">
                        <i class="nav-icon fas fa-flag"></i>
                        <p>
                            Reports
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="<?php echo base_url() ?>slides" class="nav-link
                    <?php
                    if (isset($navi_active) && $navi_active == 'slides') {
                        echo 'active';
                    }
                    ?>
                       ">
                        <i class="nav-icon fab fa-slideshare"></i>
                        <p>
                            Slides
                        </p>
                    </a>
                </li>

                <li class="nav-item ">
                    <a href="<?php echo base_url() ?>User" class="nav-link
                    <?php
                    if (isset($navi_active) && $navi_active == 'User') {
                        echo 'active';
                    }
                    ?>
                       ">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            Users
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
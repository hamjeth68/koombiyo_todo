<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view("include/head"); ?>
    </head>
    <body class="hold-transition sidebar-mini layout-fixed text-sm">
        <div class="wrapper">
            <!-- Navbar -->
            <?php $this->load->view("include/navbar"); ?>
            <!-- /.navbar -->

            <!-- Main Sidebar Container -->
            <?php $this->load->view("include/m_sidebar"); ?>

            <!-- /.content-wrapper -->
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1 class="m-0 text-dark">KoombiyoIT Support Centers</h1>
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item active">Support Center HotLine:- 011 221 331</li>
                                </ol>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
              

                <!--jeta todo-->
                <!-- /.content-header -->
                
                <!-- Main content -->
                
                <section class="content">
                    <!-- Recent Works -->
                    <div class="card card-default">
                        <div class="card-header border-transparent">
                            <h3 class="card-title">Recent Tasks</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>

                        <div class="card-body p-0">
                            <div class="row">
                                <div class="col-md-6">
                                    <ul class="nav nav-pills flex-column">
                                        <li class="nav-item">
                                            <a href="#" class="nav-link">
                                                Total Tasks November:

                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="<?php echo base_url() ?>Orders" class="nav-link">
                                                Total tasks done by  2019:

                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="<?php echo base_url() ?>" class="nav-link">
                                                Total Draw Tasks:

                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="<?php echo base_url() ?>" class="nav-link">
                                                Total Number of Customers:

                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <ul class="nav nav-pills flex-column">
                                        <li class="nav-item">
                                            <a href="<?php echo base_url() ?>" class="nav-link">
                                                Total Number of Orders:
<!--                                                <span class="float-right text-danger">
                                                    <i class="fas fa-arrow-down text-sm"></i>
                                                    12%</span>-->
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="<?php echo base_url() ?>" class="nav-link">
                                                Monthly Average Number of Orders:

                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="<?php echo base_url() ?>" class="nav-link">
                                                Average Order Amount:
                                                <span class="float-right text-warning">
<!--                                                    <i class="fas fa-arrow-left text-sm"></i> 0%-->
                                                </span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="<?php echo base_url() ?>" class="nav-link">
                                                Highest Order Amount:                                              
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!-- recent works -->
                    <!-- TABLE: LATEST works -->
                    <section class="content">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <button type="button"  style="background-color: #0388fc" class="btn btn-add" data-toggle="modal" data-target="#UserAdd">
                                                <i class="fas fa-plus-square"></i>
                                                Add
                                            </button>
                                        </div>
                                        <div>
                                        <select class="select-item">
                                                <option value="0">Select a task:</option>
                                                <option value="1">Audi</option>
                                                <option value="2">BMW</option>
                                                <option value="3">Citroen</option>
                                                <option value="4">Ford</option>
                                                <option value="5">Honda</option>
                                                <option value="6">Jaguar</option>
                                                <option value="7">Land Rover</option>
                                                <option value="8">Mercedes</option>
                                                <option value="9">Mini</option>
                                                <option value="10">Nissan</option>
                                                <option value="11">Toyota</option>
                                                <option value="12">Volvo</option>
                                        </select>
                                        </div>
                                        <!-- body of card-->
                                        <div class="card-body"> 
                                            <table id="datatb1" class="table table-bordered table-hover">                                      
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Email Address</th>
                                                        <th>Contact Number</th>
                                                        <th>Status</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Misc</td>
                                                        <td>PSP browser</td>
                                                        <td>PSP</td>
                                                        <td>-</td>

                                                    </tr>
                                                    <tr>
                                                        <td>Other browsers</td>
                                                        <td>All others</td>
                                                        <td>-</td>
                                                        <td>-</td>

                                                    </tr>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Contact Number</th>
                                                        <th>Email Address</th>
                                                        <th>Status</th>

                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
            </div>
            
            <!-- Modal -->
            <div class="modal fade" id="UserAdd" tabindex="-1" role="dialog" aria-labelledby="UserAddLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h5 class="modal-title" id="UserAddLabel">Add User</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <!--form section-->
                            <form>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                </div>

                                <div class="form-group">
                                    <label for="Name1">Name</label>
                                    <input type="Name" class="form-control" id="Name1" placeholder="Name">
                                    <small id="Name1" class="form-text text-muted">only one name required.</small>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password</label>
                                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                </div>

                                <div class="form-group">
                                    <label for="Number">Contact Number</label>
                                    <input type="Name" class="form-control" id="Number" placeholder="Contact Number">
                                    <small id="Number" class="form-text text-muted">contact number required</small>
                                </div>
                                <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                    <label class="form-check-label" for="exampleCheck1">Check me out</label>
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>

                            <!--/form end-->

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Add</button>
                        </div>
                    </div>
                </div>
            </div>
              </div>
        
            <!--/Modal -->
         
            <?php $this->load->view("include/footer"); ?>
            <?php $this->load->view("include/script"); ?>
            <!-- DataTables -->
            <script src="<?php echo base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
            <script src="<?php echo base_url() ?>assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
            <script src="<?php echo base_url() ?>assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
            <script src="<?php echo base_url() ?>assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

            <!-- page script -->
            <script>
                $(function () {
                    $("#example1").DataTable({
                        "responsive": true,
                        "autoWidth": true,
                    });
                    $('#datatb1').DataTable({
                        "paging": true,
                        "lengthChange": true,
                        "searching": true,
                        "ordering": true,
                        "info": true,
                        "autoWidth": true,
                        "responsive": true,

                    });
                });


            </script>
    </body>
</html>






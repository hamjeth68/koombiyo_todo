<html>
    <head>
        <?php $this->load->view("include/head"); ?>

        <!-- DataTables  -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/DataTables-bs4/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/DataTables-responsive/css/responsive.bootstrap4.min.css">




    </head>
    <body class="hold-transition sidebar-mini layout-fixed text-sm">
        <div class="wrapper">
            <!-- Navbar -->
            <?php $this->load->view("include/navbar"); ?>
            <!-- /.navbar -->

            <!-- Main Sidebar Container -->
            <?php $this->load->view("include/m_sidebar"); ?>

            <!-- /.content-wrapper -->
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <?php $this->load->view("include/content-header"); ?>
                <!-- /.content-header -->

                <!-- Main content -->
                <section class="content"> 


                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">

                                    </div>
                                    <!-- body of card-->
                                    <div class="card-body">                          
                                        <table id="datatb1" class="table table-bordered table-hover">                                      
                                            <thead>
                                                <tr>
                                                    <th>Customer Name</th>
                                                    <th>Order ID</th>
                                                    <th>Issue Description</th>
                                                    <th>Status</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Misc</td>
                                                    <td>PSP browser</td>
                                                    <td>PSP</td>
                                                    <td>
                                                        <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                                                            <div class="btn-group mr-2" role="group" aria-label="First group">
                                                                <button type="button" class="btn btn-secondary" style="background-color: #007bff" id="btn-edit" data-toggle="modal" data-target="#myModal">
                                                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                    <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                                                    <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                                                    </svg>                                                                   
                                                                </button>
                                                                <button type="button" class="btn btn-secondary" id="btn-add" data-toggle style="background-color: #007bff">
                                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>

                                                                </button>
                                                                <button type="button" class="btn btn-secondary" id="btn-delete" style="background-color: #007bff">
                                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td>Other browsers</td>
                                                    <td>All others</td>
                                                    <td>-</td>
                                                    <td><div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                                                            <div class="btn-group mr-2" role="group" aria-label="First group">
                                                                <button type="button" class="btn btn-secondary" style="background-color: #007bff" id="btn-edit" data-toggle="modal" data-target="#myModal">
                                                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                                    <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                                                    <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                                                    </svg>                                                                   
                                                                </button>
                                                                <button type="button" class="btn btn-secondary" id="btn-add" data-toggle style="background-color: #007bff">
                                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>

                                                                </button>
                                                                <button type="button" class="btn btn-secondary" id="btn-delete" style="background-color: #007bff">
                                                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </td>

                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Customer Name</th>
                                                    <th>Order ID</th>
                                                    <th>Issue Description</th>
                                                    <th>Status</th>

                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <form>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Customer Name</label>
                                <input type="email" class="form-control" id="Name1" aria-describedby="emailHelp" placeholder="Customer Name">
                            </div>

                            <div class="form-group">
                                <label for="Number">Order ID</label>
                                <input type="Name" class="form-control" id="Order ID" placeholder="Order ID">
                            </div>

                            <div class="form-group">
                                <label for="Number">Issue Description</label>
                                <input type="Name" class="form-control" id="Desp" placeholder="Issue Description">
                            </div>

                            <div class="form-group form-check">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                <label class="form-check-label" for="exampleCheck1">Check me out</label>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <?php $this->load->view("include/footer"); ?>
            <?php $this->load->view("include/script"); ?>
            <!-- DataTables -->
            <script src="<?php echo base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
            <script src="<?php echo base_url() ?>assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
            <script src="<?php echo base_url() ?>assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
            <script src="<?php echo base_url() ?>assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>



            <!-- page script -->
            <script>
                $(function () {
                    $("#example1").DataTable({
                        "responsive": true,
                        "autoWidth": true,
                    });
                    $('#datatb1').DataTable({
                        "paging": true,
                        "lengthChange": true,
                        "searching": true,
                        "ordering": true,
                        "info": true,
                        "autoWidth": true,
                        "responsive": true,
                    });
                });
            </script>

    </body>
</html>






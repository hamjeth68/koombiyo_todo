<html>
    <head>
        <?php $this->load->view("include/head"); ?>

        <!-- DataTables  -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/DataTables-bs4/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/DataTables-responsive/css/responsive.bootstrap4.min.css">




    </head>
    <body class="hold-transition sidebar-mini layout-fixed text-sm">
        <div class="wrapper">
            <!-- Navbar -->
            <?php $this->load->view("include/navbar"); ?>
            <!-- /.navbar -->

            <!-- Main Sidebar Container -->
            <?php $this->load->view("include/m_sidebar"); ?>

            <!-- /.content-wrapper -->
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <?php $this->load->view("include/content-header"); ?>
                <!-- /.content-header -->

                <!-- Main content -->
                <section class="content">


                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <button type="button"  style="background-color: #007bff" class="btn btn-add" data-toggle="modal" data-target="#UserAdd">
                                            <i class="fas fa-plus-square"></i>
                                            Add Units
                                        </button>
                                    </div>
                                    <!-- body of card-->
                                    <div class="card-body">                          
                                        <table id="datatb1" class="table table-bordered table-hover">                                      
                                            <thead>
                                                <tr>
                                                    <th>Number of Units</th>
                                                    <th>Product Category</th>
                                                    <th>Contact Number</th>
                                                    <th>Status</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Misc</td>
                                                    <td>PSP browser</td>
                                                    <td>PSP</td>
                                                    <td>-</td>

                                                </tr>
                                                <tr>
                                                    <td>Other browsers</td>
                                                    <td>All others</td>
                                                    <td>-</td>
                                                    <td>-</td>

                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Number of Units</th>
                                                    <th>Product Category</th>
                                                    <th>Contact Number</th>
                                                    <th>Status</th>

                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>

            <!-- Modal -->
            <div class="modal fade" id="UserAdd" tabindex="-1" role="dialog" aria-labelledby="UserAddLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="UserAddLabel">Add Units</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <!--form section-->
                            <form>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Number of Units</label>
                                    <input type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Unit Numbers">

                                </div>

                                <div class="form-group">
                                    <label for="productID" >Product ID</label>
                                    <input type="numbers" class="form-control" id="Name1" placeholder="Enter Product ID">
                                </div>

                                <div class="form-group">
                                    <label for="Number">Contact Number</label>
                                    <input type="Name" class="form-control" id="Number" placeholder="Contact Number">
                                </div>
                                <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                    <label class="form-check-label" for="exampleCheck1">Check me out</label>
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>

                            <!--/form end-->

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Add</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--/Modal -->
            <?php $this->load->view("include/footer"); ?>
            <?php $this->load->view("include/script"); ?>
            <!-- DataTables -->
            <script src="<?php echo base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
            <script src="<?php echo base_url() ?>assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
            <script src="<?php echo base_url() ?>assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
            <script src="<?php echo base_url() ?>assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>



            <!-- page script -->
            <script>
                $(function () {
                    $("#example1").DataTable({
                        "responsive": true,
                        "autoWidth": true,
                    });
                    $('#datatb1').DataTable({
                        "paging": true,
                        "lengthChange": true,
                        "searching": true,
                        "ordering": true,
                        "info": true,
                        "autoWidth": true,
                        "responsive": true,
                    });
                });
            </script>

    </body>
</html>






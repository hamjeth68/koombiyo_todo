<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view("admin/common/head");?>
  <!-- Datatable-->
  <link rel="stylesheet" href="<?php echo base_url();?>thems/plugins/DataTables/datatables.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>thems/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>thems/plugins/bootstrap-modal/css/bootstrap-modal.css"/>
  <link rel="stylesheet" href="<?php echo base_url()?>thems/plugins/toastr/build/toastr.css"/>
  <link rel="stylesheet" href="<?php echo base_url()?>thems/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>thems/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
</head>
<body class="hold-transition sidebar-mini layout-fixed text-sm">
<div class="wrapper">
  <?php $this->load->view("admin/common/header"); ?>
  <?php $this->load->view("admin/common/sidebar"); ?>
  <div class="content-wrapper">
    <?php $this->load->view("admin/common/content-header"); ?>
    <section class="content">
        <div class="container-fluid">
			 <div class="row">
				<div class="col-xs-12 col-sm-12 col-lg-12">
					<div class="card">
						<div class="card-header">
							<button class="btn btn-primary btn-sm btn-flat" onClick="load_allowances_add();"><i class="fa fa-plus"></i> New</button>
						</div>
						<div class="card-body">
							<table id="users" class="table table-bordered table-striped" width="100%">
								<thead>
									<tr>
									  <th>Name</th>
									  <th>Username</th>
									  <th>E-mail</th>
									  <th>Phone Number</th>
									  <th>Status</th>
									  <th>Tools</th>
									</tr>
								</thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>Sajith</td>
									  <td>sajith1234</td>
									  <td>sajith.it@koombiyodelivery.lk</td>
                                                                          <td>0777813079</td>
									  <td>Active</td>
                                                                          <td><button class="btn btn-sm"><li class="fas fa-edit"></li></button></td>
									</tr>
                                                                </tbody>
								
							</table>
						</div>
					</div>
				</div>	
		    </div>
		</div>
    </section>
  </div>
  <?php $this->load->view("admin/common/footer"); ?>
   <div id="ajax-modal" class="modal"></div>
  <script src="<?php echo base_url();?>thems/plugins/DataTables/datatables.min.js"></script>
  <script src="<?php echo base_url();?>thems/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
  <script src="<?php echo base_url();?>thems/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
  <script src="<?php echo base_url();?>thems/plugins/validation/dist/jquery.validate.js"></script>
  <script src="<?php echo base_url();?>thems/plugins/select2/js/select2.full.min.js"></script>
  <script src="<?php echo base_url();?>thems/plugins/toastr/toastr.js"></script>
  <script>
	$(document).ready( function () {
    $('#users').DataTable();
} );
  </script>
</body>
</html>


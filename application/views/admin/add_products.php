<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view("admin/common/head"); ?>
        <!-- Datatable-->
        <link rel="stylesheet" href="<?php echo base_url() ?>thems/plugins/toastr/build/toastr.css"/>
        <link rel="stylesheet" href="<?php echo base_url() ?>thems/plugins/select2/css/select2.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>thems/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>thems/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    </head>
    <body class="hold-transition sidebar-mini layout-fixed text-sm">
        <div class="wrapper">
            <?php $this->load->view("admin/common/header"); ?>
            <?php $this->load->view("admin/common/sidebar"); ?>
            <div class="content-wrapper">
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1 class="m-0 text-dark"><?php echo $title; ?></h1>
                            </div><!-- /.col -->
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item active"><?php echo $sub_title; ?></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <section class="content"> 
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <?php echo $sub_title; ?>
                                    </div>
                                    <div class="card-body">
                                        <form role="form" class="form-horizontal" id="product_form" action="#" method="post"> 
                                            <input type="hidden" value="<?php echo $type; ?>" id="type" name="type" readonly="readonly">
                                            <input type="hidden" value="<?php echo $product_id; ?>" id="product_id" name="product_id" readonly="readonly">
                                            <div class="row">			
                                                <div class="col-xs-12 col-sm-6 col-md-6">
                                                    <div class="form-group">
                                                        <label>Name *</label>
                                                        <input type="text" class="form-control" name="product_name" id="product_name" <?php
                                                        if (isset($product_data['product_name'])) {
                                                            echo 'value="' . $product_data['product_name'] . '"';
                                                        }
                                                        ?> style="width:100%;" required="required"/>  
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Category *</label>
                                                        <select class="form-control select2" name="category_id" id="category_id"  style="width:100%;" > 
                                                            <option selected=""></option>
                                                            <?php
                                                            $categories = $this->Public_model->get_cats();
                                                            foreach ($categories as $row) {
                                                                ?>
                                                                <option value="volvo"><?php echo $row['category_name'] ?></option>
                                                            <?php } ?>   
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Sub Category *</label>
                                                        <select class="form-control select2" name="sub_category_id" id="sub_category_id"  style="width:100%;" > 

                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-6">  
                                                    <div class="form-group">
                                                        <label>Unit *</label>
                                                        <select class="form-control select2" name="unit_id" id="unit_id"  style="width:100%;" > 

                                                        </select>
                                                    </div>
                                                    <!--                      <div class="form-group clearfix">
                                                                            <div class="icheck-success d-inline">
                                                                              <input type="checkbox" id="for_quotation" name="for_quotation" checked>
                                                                              <label for="for_quotation">
                                                                                  For Quotation
                                                                              </label>
                                                                            </div>
                                                                            <div class="icheck-success d-inline">
                                                                              <input type="checkbox" id="for_sale" name="for_sale">
                                                                              <label for="for_sale">
                                                                                For Sale
                                                                              </label>
                                                                            </div>
                                                                          </div>-->
                                                    <div class="form-group">
                                                        <label>Price *</label>
                                                        <input type="number" class="form-control" name="price" id="price" <?php
                                                        if (isset($product_data['price'])) {
                                                            echo 'value="' . $product_data['price'] . '"';
                                                        }
                                                        ?> style="width:100%;" required="required"/>  
                                                    </div>
                                                </div>
                                            </div>
                                        </form>  
                                    </div>
                                    <div class="card-footer">
                                        <button type="button" class="btn btn-danger btn-flat pull-left" data-dismiss="modal"><i class="fas fa-times"></i> Close</button>
                                        <input type="submit" id="add" name="add" value="<?php echo $btn_Text; ?>" class="btn btn-success btn-flat float-right">
                                    </div>
                                </div>
                            </div>	
                        </div>
                    </div>
                </section>
            </div>
            <?php $this->load->view("admin/common/footer"); ?>
            <div id="ajax-modal" class="modal"></div>
            <script src="<?php echo base_url(); ?>thems/plugins/validation/dist/jquery.validate.js"></script>
            <script src="<?php echo base_url(); ?>thems/plugins/select2/js/select2.full.min.js"></script>
            <script src="<?php echo base_url(); ?>thems/plugins/toastr/toastr.js"></script>
            <script>
                $(document).ready(function () {
                    $('#category_id').select2();
                    $('#sub_category_id').select2();
                    $('#unit_id').select2();
                });
            </script>
    </body>
</html>
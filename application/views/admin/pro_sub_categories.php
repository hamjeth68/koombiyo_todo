<!DOCTYPE html>
<html>
<head>
  <?php $this->load->view("admin/common/head");?>
  <!-- Datatable-->
  <link rel="stylesheet" href="<?php echo base_url();?>thems/plugins/DataTables/datatables.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>thems/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>thems/plugins/bootstrap-modal/css/bootstrap-modal.css"/>
  <link rel="stylesheet" href="<?php echo base_url()?>thems/plugins/toastr/build/toastr.css"/>
  <link rel="stylesheet" href="<?php echo base_url()?>thems/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>thems/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
</head>
<body class="hold-transition sidebar-mini layout-fixed text-sm">
<div class="wrapper">
  <?php $this->load->view("admin/common/header"); ?>
  <?php $this->load->view("admin/common/sidebar"); ?>
  <div class="content-wrapper">
    <?php $this->load->view("admin/common/content-header"); ?>
    <section class="content">
        <div class="container-fluid">
			 <div class="row">
				<div class="col-xs-12 col-sm-12 col-lg-12">
					<div class="card">
						<div class="card-header">
                                                    <input type="hidden" id="category_id" name="category_id" value="<?php echo $category_id;?>" />
                                                    <button class="btn btn-primary btn-sm btn-flat pull-right" onClick="load_sub_category_add();"><i class="fa fa-plus"></i> New</button>
						</div>
						<div class="card-body">
							<table id="sub_categories" class="table table-bordered table-striped" width="100%">
								<thead>
                                                                        <tr>
                                                                            <th colspan="3" style="text-align:center;color:#fff;background:#17a2b8;"><?php echo $category_details['category_name'];?></th>
                                                                        </tr>
									<tr>
									  <th>Name</th>
									  <th>Short Code</th>
									  <th>Tools</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
									  <th>Name</th>
									  <th>Short Code</th>
									  <th>Tools</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>	
		    </div>
		</div>
    </section>
  </div>
  <?php $this->load->view("admin/common/footer"); ?>
   <div id="ajax-modal" class="modal"></div>
  <script src="<?php echo base_url();?>thems/plugins/DataTables/datatables.min.js"></script>
  <script src="<?php echo base_url();?>thems/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
  <script src="<?php echo base_url();?>thems/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
  <script src="<?php echo base_url();?>thems/plugins/validation/dist/jquery.validate.js"></script>
  <script src="<?php echo base_url();?>thems/plugins/select2/js/select2.full.min.js"></script>
  <script src="<?php echo base_url();?>thems/plugins/toastr/toastr.js"></script>
  <script>
	$(document).ready(function(){
            load_sub_categories_table();
        });
        function load_sub_category_add(){
            var type ="add";
            var $modal = $('#ajax-modal');
            var category_id = $("#category_id").val();
            $('body').modalmanager('loading');	
            setTimeout(function () {
                    $modal.load('<?php echo base_url("admin/Products/sub_category_modal_load?type=");?>'+type+'&category_id='+category_id,'',function (){
                            $modal.modal();
                    });
            },10);
	}
        function load_sub_categories_table(){
            $("#sub_categories").DataTable({
                    deferRender:!0,
                    destroy:!0,
                    responsive:!0,
                    orderable:!0,
                    stateSave:!0,
                    pageLength:25,
                    processing:!0,
                    serverSide:!0,
                    columnDefs: [
                           { orderable: false, targets: 2 },
                    ],
                    ajax:{
                        url:"<?php echo base_url()?>admin/Products/sub_categories_list",
                        type:"POST",
                        data:{
                           category_id:$("#category_id").val()
                        }
                    }
           });
        }
	function deactivate_sub_category(sub_category_id){
	    $.post("<?php echo base_url();?>admin/Products/deactivate_sub_category",{sub_category_id:sub_category_id},function(data){
		var obj = jQuery.parseJSON(data);
		if (obj.status==0){
		    toastr.error(obj.msg);	
		}else if(obj.status==1){
		    toastr.success(obj.msg);	
		    load_sub_categories_table();
		};
	    });
	}
	function activate_sub_category(sub_category_id){
	    $.post("<?php echo base_url();?>admin/Products/activate_sub_category",{sub_category_id:sub_category_id},function(data){
		var obj = jQuery.parseJSON(data);
		if (obj.status==0){
		    toastr.error(obj.msg);	
		}else if(obj.status==1){
		    toastr.success(obj.msg);	
		    load_sub_categories_table();
		};
	    });
	}
	function update_sub_category(sub_category_id,category_id){
	    var type ="update";
	    var $modal = $('#ajax-modal');
	    $('body').modalmanager('loading');	
	    setTimeout(function () {
		    $modal.load('<?php echo base_url("admin/Products/sub_category_modal_load?type=");?>'+type+'&sub_category_id='+sub_category_id+'&category_id='+category_id,'',function (){
			    $modal.modal();
		    });
	    },10);
	}
	function delete_sub_category(sub_category_id){
	     $.post("<?php echo base_url();?>admin/Products/delete_sub_category",{sub_category_id:sub_category_id},function(data){
		var obj = jQuery.parseJSON(data);
		if (obj.status==0){
		    toastr.error(obj.msg);	
		}else if(obj.status==1){
		    toastr.success(obj.msg);	
		    load_sub_categories_table();
		};
	     });
	}
  </script>
</body>
</html>
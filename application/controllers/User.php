
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    var $title = "Koombiyo Super";
    var $pagen = "User";
    var $navi_active = 'User';

    public function index() {
        $data['title'] = $this->title;
        $data['pagen'] = $this->pagen;
        $data['navi_active'] = $this->navi_active;
        $this->load->view('user', $data);
    }

}

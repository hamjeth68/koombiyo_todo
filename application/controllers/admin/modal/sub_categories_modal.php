<style>
.error{
	color:#FF0000;
	
}
.form-control.error{
	border:1px solid #FF3737;
}
</style>
<form role="form" class="form-horizontal" id="sub_category_form" action="#" method="post">   
        <div class="modal-header">
        	<h4 class="modal-title"><?php echo $page_title;?></h4>
        	<button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <h6>With this * mark all fields are required</h6>
            <input type="hidden" value="<?php echo $type;?>" id="type" name="type" readonly="readonly">
            <input type="hidden" value="<?php echo $category_id;?>" id="category_id" name="category_id" readonly="readonly">
            <input type="hidden" value="<?php echo $sub_category_id;?>" id="sub_category_id" name="sub_category_id" readonly="readonly">
            <div class="form-group">
                <div class="row">
                  <div class="col-12 col-sm-12">
                        <div class="form-group">
                            <label>Name *</label>
                            <input type="text" class="form-control" name="sub_category_name" id="sub_category_name" <?php if(isset($sub_category_data['sub_category_name'])){ echo 'value="'.$sub_category_data['sub_category_name'].'"';}?> style="width:100%;" required="required"/>  
                        </div>
                        <div class="form-group">
                            <label>Short Code *</label>
                            <input type="text" class="form-control" name="short_code" id="short_code" <?php if(isset($sub_category_data['short_code'])){ echo 'value="'.$sub_category_data['short_code'].'"';}?> style="width:100%;" required="required"/>  
                        </div>
                  </div>
                </div>
            </div>		
        </div>
        <div class="modal-footer">
                <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                <input type="submit" id="add" name="add" value="<?php echo $btn_Text; ?>" class="btn btn-success btn-flat">
        </div>
</form>
<script>
$("#sub_category_form").validate({
    errorElement: 'div',
    rules: {
        sub_category_name:"required",
        short_code:"required",
    },
    submitHandler: function (form) {
        insertSubcategoriesdata(form);
    }
});
function insertSubcategoriesdata(form){
	$("#add").attr("disabled", true);
        $.ajax({
            url: "<?php echo base_url();?>admin/Products/save_sub_category_data",
            type: "POST",             
            data: new FormData(form),
            contentType: false,       
            cache: false,            
            processData:false,        
            success: function(data){
                var obj = jQuery.parseJSON(data);
                if (obj.status==0){
                    $("#add").attr("disabled",false);
                    toastr.error(obj.message);	
                }else if(obj.status==1){
                    toastr.success(obj.message);	
                    $('div#ajax-modal').modal('hide');
                    load_sub_categories_table();
                };
            }	
        });
}
</script>
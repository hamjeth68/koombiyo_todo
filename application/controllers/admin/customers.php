<!DOCTYPE html>
<html>
    <head>
        <?php $this->load->view("admin/common/head"); ?>
        <!-- Datatable-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>thems/plugins/DataTables/datatables.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>thems/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>thems/plugins/bootstrap-modal/css/bootstrap-modal.css"/>
        <link rel="stylesheet" href="<?php echo base_url() ?>thems/plugins/toastr/build/toastr.css"/>
        <link rel="stylesheet" href="<?php echo base_url() ?>thems/plugins/select2/css/select2.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>thems/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    </head>
    <body class="hold-transition sidebar-mini layout-fixed text-sm">
        <div class="wrapper">
            <?php $this->load->view("admin/common/header"); ?>
            <?php $this->load->view("admin/common/sidebar"); ?>
            <div class="content-wrapper">
                <?php $this->load->view("admin/common/content-header"); ?>
                <section class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-lg-12">
                                <div class="card">
                                    <div class="card-header">
<!--                                        <button class="btn btn-primary btn-sm btn-flat" onClick="load_allowances_add();"><i class="fa fa-plus"></i> New</button>-->
                                    </div>
                                    <div class="card-body">
                                        <table id="customers" class="table table-bordered table-striped" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Name</th>
                                                    <th>Phone</th>
                                                    <th>Email</th>
                                                    <th>Tools</th>
                                                </tr>
                                            </thead>
                                            
                                            <tfoot>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Name</th>
                                                    <th>Phone</th>
                                                    <th>Email</th>
                                                    <th>Tools</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>	
                        </div>
                    </div>
                </section>
            </div>
            <?php $this->load->view("admin/common/footer"); ?>
            <div id="ajax-modal" class="modal"></div>
            <script src="<?php echo base_url(); ?>thems/plugins/DataTables/datatables.min.js"></script>
            <script src="<?php echo base_url(); ?>thems/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
            <script src="<?php echo base_url(); ?>thems/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
            <script src="<?php echo base_url(); ?>thems/plugins/validation/dist/jquery.validate.js"></script>
            <script src="<?php echo base_url(); ?>thems/plugins/select2/js/select2.full.min.js"></script>
            <script src="<?php echo base_url(); ?>thems/plugins/toastr/toastr.js"></script>
            <script>
            $(document).ready(function () {
                load_products_table();
            });
            function load_products_table() {
                $("#customers").DataTable({
                    deferRender: !0,
                    destroy: !0,
                    responsive: !0,
                    orderable: !0,
                    stateSave: !0,
                    pageLength: 25,
                    processing: !0,
                    serverSide: !0,
                    columnDefs: [
                        {orderable: false, targets: 2},
                    ],
                    ajax: {
                        url: "<?php echo base_url() ?>admin/Customers/customers_list",
                        type: "POST",
                    }
                });
            }
            </script>    
    </body>
</html>


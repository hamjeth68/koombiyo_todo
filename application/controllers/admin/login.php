<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Koombiyo HR | Log in</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo base_url();?>thems/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>thems/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>thems/dist/css/adminlte.min.css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="<?php echo base_url();?>thems/dist/img/logo.png"><b>Koombiyo Super</b></a>
  </div>
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>
      <form method="POST"  class="form-horizontal wow fadeIn animated" role="form" onSubmit="return false;" id="login_form" name="login_form">
        <div class="input-group mb-3">
          <input type="mail" class="form-control" placeholder="User mail" id="user_name" name="user_name">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Password" id="password" name="password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div style="margin-top:10px" class="resultlogin"></div>
</div>

<script src="<?php echo base_url();?>thems/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>thems/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url();?>thems/dist/js/adminlte.min.js"></script>
<script>
$(function () {
	$("#login_form").on('submit',function(){
		$(".resultlogin").html("<div class='alert alert-info loading wow fadeOut animated'>Hold On...</div>");
		$.post("<?php echo base_url()?>admin/Login/login_to_system",$("#login_form").serialize(), function(response){
			var resp = $.parseJSON(response);
			console.log(resp);
			if(!resp.status){
				$(".resultlogin").html("<div class='alert alert-danger loading wow fadeIn animated'>"+resp.message+"</div>");
			}else{
				$(".resultlogin").html("<div class='alert alert-success login wow fadeIn animated'>Redirecting Please Wait...</div>");
				window.location.replace(resp.url);
			}
		});
	});
});
</script>
</body>
</html>

<aside class="main-sidebar elevation-4 sidebar-no-expand sidebar-light-info">
    <a href="<?php echo base_url(); ?>dashboard" class="brand-link navbar-info">
        <img src="<?php echo base_url(); ?>thems/dist/img/logo.png" alt="Koombiyo Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light text-white">Koombiyo Super</span>
    </a>
    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">   
                <?php if (!empty($this->session->userdata['staff_logged_in']['user_image'])) { ?>  
                    <img src="<?php echo $this->session->userdata['staff_logged_in']['user_image']; ?>" class="img-circle elevation-2" alt="User Image">
                <?php } else { ?>
                    <img src="<?php echo base_url() ?>thems/images/common_user.png" class="img-circle elevation-2" alt="User Image">
                <?php } ?>
            </div>
            <div class="info">
                <a href="#" class="d-block">Sajith</a>
            </div>
        </div>
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column  nav-flat nav-compact nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">
                <!--dashboard-->
                <li class="nav-item">
                    <a href="<?php echo base_url(); ?>admin/dashboard" class="nav-link <?php if (isset($navi_active) && $navi_active == 'dashboard') {
                    echo 'active';
                } ?>">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                <!--dashboard-->
                <!--products-->
                <li class="nav-item has-treeview <?php if (isset($navi_active) && $navi_active == 'products') {
                    echo 'menu-open';
                } ?>">
                    <a href="#" class="nav-link <?php if (isset($navi_active) && $navi_active == 'products') {
                    echo 'active';
                } ?>">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            Products
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?php echo base_url(); ?>admin/products" class="nav-link <?php if (isset($sub_active) && $sub_active == 'products') {
                    echo 'active';
                } ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Products</p>
                            </a>
                        </li>  
                        <li class="nav-item">
                            <a href="<?php echo base_url(); ?>admin/categories" class="nav-link <?php if (isset($sub_active) && $sub_active == 'categories') {
                    echo 'active';
                } ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Categories</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo base_url(); ?>admin/units" class="nav-link <?php if (isset($sub_active) && $sub_active == 'units') {
                    echo 'active';
                } ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Units</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <!--products-->
                <!--customers-->
                <li class="nav-item">
                    <a href="<?php echo base_url(); ?>admin/customers" class="nav-link <?php if (isset($navi_active) && $navi_active == 'customers') {
                    echo 'active';
                } ?>">
                        <i class="nav-icon fas fa-user-circle"></i>
                        <p>
                            Customers
                        </p>
                    </a>
                </li>
                <!--customers-->
                <!--orders-->
                <li class="nav-item">
                    <a href="<?php echo base_url(); ?>admin/orders" class="nav-link <?php if (isset($navi_active) && $navi_active == 'orders') {
                    echo 'active';
                } ?>">
                        <i class="nav-icon fas fa-shopping-cart"></i>
                        <p>
                            Orders
                        </p>
                    </a>
                </li>
                <!--orders-->
                <!--reports-->
                <li class="nav-item has-treeview <?php if (isset($navi_active) && $navi_active == 'reports') {
                    echo 'menu-open';
                } ?>">
                    <a href="#" class="nav-link <?php if (isset($navi_active) && $navi_active == 'reports') {
                    echo 'active';
                } ?>">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            Reports
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?php echo base_url(); ?>admin/products" class="nav-link <?php if (isset($sub_active) && $sub_active == 'products') {
                    echo 'active';
                } ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Products</p>
                            </a>
                        </li>  
                        <li class="nav-item">
                            <a href="<?php echo base_url(); ?>admin/categories" class="nav-link <?php if (isset($sub_active) && $sub_active == 'categories') {
                    echo 'active';
                } ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Categories</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo base_url(); ?>admin/units" class="nav-link <?php if (isset($sub_active) && $sub_active == 'units') {
                    echo 'active';
                } ?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Units</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <!--reports-->
                <!--users-->
                <li class="nav-item">
                    <a href="<?php echo base_url(); ?>admin/slides" class="nav-link <?php if (isset($navi_active) && $navi_active == 'slides') {
                    echo 'active';
                } ?>">
                        <i class="nav-icon fas fa-image"></i>
                        <p>
                            Slides
                        </p>
                    </a>
                </li>
                <!--users-->
                <!--users-->
                <li class="nav-item">
                    <a href="<?php echo base_url(); ?>admin/users" class="nav-link <?php if (isset($navi_active) && $navi_active == 'users') {
                    echo 'active';
                } ?>">
                        <i class="nav-icon fas fa-user-circle"></i>
                        <p>
                            Users
                        </p>
                    </a>
                </li>
                <!--users-->
        </nav>
    </div>
</aside>
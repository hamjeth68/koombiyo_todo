<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {

    var $title = "Koombiyo Super";
    var $pagen = "Product";
    var $navi_active = 'product';

    public function index() {
        $data['title'] = $this->title;
        $data['pagen'] = $this->pagen;
        $data['navi_active'] = $this->navi_active;
        $this->load->view('products', $data);
    }

    function product() {
        $data['title'] = $this->title;
        $data['pagen'] = $this->pagen;
        $data['navi_active'] = $this->navi_active;
        $data['sub_navi_active'] = "products";
        $this->load->view('products', $data);
    }

    function categories() {
        $data['title'] = $this->title;
        $data['pagen'] = $this->pagen;
        $data['navi_active'] = $this->navi_active;
        $data['sub_navi_active'] = "categories";
        $this->load->view('categories', $data);
    }

    function units() {
        $data['title'] = $this->title;
        $data['pagen'] = $this->pagen;
        $data['navi_active'] = $this->navi_active;
        $data['sub_navi_active'] = "units";
        $this->load->view('units', $data);
    }

}

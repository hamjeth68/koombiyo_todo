<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {

    var $title = "Koombiyo Super";
    var $pagen = "Orders";
    var $navi_active = 'orders';
    
    

    public function index() {
        $data['title'] = $this->title;
        $data['pagen'] = $this->pagen;
        $data['navi_active'] = $this->navi_active;
        $data['navi_active'] = 'orders';
        $this->load->view('orders', $data);
    }

}


<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    var $title = "Koombiyo Super";
    var $pagen = "Dashboard";
    var $navi_active = 'dashboard';

    public function index() {
        $data['title'] = $this->title;
        $data['pagen'] = $this->pagen;
        $data['navi_active'] = $this->navi_active;
        $this->load->view('dashboard', $data);
    }

}
